const express = require('express')
var cors = require('cors')
const app = express()
app.use(cors())
app.use(express.json())
const port = 3300

const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://localhost:9200' })



app.get('/populate', async (req, res) => {
  const fs = require('fs');
  let rawdata = fs.readFileSync('db_meta.json');
  let db_meta = JSON.parse(rawdata);
  es_bulk_payload = [];
  db_meta.forEach(d=>{
    es_bulk_payload.push({
      index: {
        _index: 'tokens'
      }
    });
    es_bulk_payload.push(d);
  });
  
  client.bulk({
    index: 'tokens',
    body: es_bulk_payload
  }, (err, response)=>{
    console.log(err, response);
    // if(err) res.send(err);
    res.send(response);
  })
});

app.post('/', async (req, res) => {
  console.log(req.body.search_text)
  let search_query = req.body.search_text;
  let {entityArray, caretPos } = req.body;

  for(let i=0;i<entityArray.length;i++){
    if(entityArray[i].start <= caretPos && caretPos <= entityArray[i].end ){
      search_query = entityArray[i].text;
      search_query = search_query.trim();
      break;
    }
  }

  /**
   * `forEach()` isn't Promise aware
   * 
   *  src: https://zellwk.com/blog/async-await-in-loops/
   */
  for(let i=0;i<entityArray.length;i++){
    let e = entityArray[i];
  
    if(e.type==='INCOMPLETE'){
      const { body: result } = await client.search({
        index: 'tokens',
        body: {
          "query": {
            "match" : {
              "name" : e.text.trim()
            }
          }
        }
      });
      
      // console.log("==>", e.text, result )
      if(result.hits && result.hits.hits.length===1){
        // console.log("===========")
        let { body: dup} = await client.search({
          index: "tokens",
          body: {
            "query": {
              "match_phrase_prefix" : {
                "name" : e.text.trim()
              }
            }
          }
        });
        console.log("==> ",dup, " ", e.text.trim())
        dup = dup.hits.hits.length;
        console.log("type ",result.hits.hits[0]._source.token_type, dup)
        // entityArray[i].type = 'COMPLETE';
        entityArray[i].type = result.hits.hits[0]._source.token_type;
        entityArray[i].isExtensible = dup===1 ? false : true;
        // result.hits.hits[0]._source
      }
    }
    else if(e.type!=='INCOMPLETE'){
      const { body: result } = await client.search({
        index: 'tokens',
        body: {
          "query": {
            "match" : {
              "name" : e.text.trim()
            }
          }
        }
      });
      if(result.hits && result.hits.hits.length!==1){
        entityArray[i].type = 'INCOMPLETE';
        entityArray[i].isExtensible = true;
        // result.hits.hits[0]._source
      }
    }
  };
  client.search({
    index: "tokens",
    body: {
      "query": {
        "match_phrase_prefix" : {
          "name" : search_query
        }
      }
    }
  }, (err, result) => {
    if (err) console.log(err);

    let temp =  [];
    // console.log(result)
    if(result && result.body && result.body.hits && result.body.hits.hits) temp = result.body.hits.hits;
    temp = temp.map(d=> d._source)
    res.send({
      suggestions: temp,
      displayTokens: entityArray
    });
  });
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))
